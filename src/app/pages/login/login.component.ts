import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { delay } from 'rxjs/operators';
import { HTTPStatus } from '../../base/interceptors';
import { SessionService } from '../../base/session.service';
import { User } from '../../models/common.models';
import { HttpClient } from '@angular/common/http';

declare var $: any;

@Component({
    selector: 'app-login-cmp',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {
    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    public HTTPActivity: boolean;
    public user: User = new User();
    returnUrl: string;
    public errorMsg: string;
    userLogged=false
    isFirefox=false
    error=null

    constructor(private httpStatus: HTTPStatus, private element: ElementRef, 
        private sessionService: SessionService, private route: ActivatedRoute,
        private router: Router,   private http : HttpClient) {
        this.sidebarVisible = false;
        this.httpStatus.getHttpStatus().pipe(delay(100)).subscribe((status: boolean) => { this.HTTPActivity = status });
     
    }
    
    ngOnInit() {
        var navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        const card = document.getElementsByClassName('card')[0];
        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            card.classList.remove('card-hidden');
        }, 700);
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.checkSession();
    }

    checkSession(){
        this.sessionService.checkSession().subscribe(resp => {
            console.log("Checking sessionInfo in login:", resp);
            if (resp.usuario) {
                this.sessionService.setSessionInfo(resp);
                if (this.returnUrl) {
                    this.router.navigate([this.returnUrl]);
                } else {
                    this.router.navigate(['dashboard']);
                }
            }
        }, resp => {
            this.errorMsg = resp.error || "Ocurrió un error en el servidor";
            //TODO: dirigir a página de error
            console.log("Error al verificar sesión:", resp)
        });
    }

    sidebarToggle() {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
       
        if (this.sidebarVisible == false) {
            setTimeout(function () {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }

    ngOnDestroy() {
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
        body.classList.remove('off-canvas-sidebar');
    }

    doLogin() {
        this.sessionService.login(this.user).subscribe(resp => {
            console.log("login", resp);
            this.userLogged = true
            //checkSession para obtener entidadList, entidad, etc
            this.checkSession()
        }, err => {
            console.log("login error", err);
            if (err.status == 401) {
                this.errorMsg = "Usuario o contraseña incorrectos";
            } else if (err.status == 409){
               this.errorMsg = err.error;
            } else {
                this.errorMsg = "Ocurrió un error en el servidor";
            }

        });
    }
}

