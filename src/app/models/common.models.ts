import { Usuario } from "app/administracion/administracion.models";


export class Empresa {
  id: number;
  codigo: string;
  ruc: string;
  razonSocial: string;
  empleador: string;
  actividad: string;
  nroPatronal: string;
  nroPatronalIps: string;
  departamento: string;
  localidad: string;
  domicilio: string;
  telefono: string;
  email: string;
  logoHeight: number
  logoWidth: number
  location: Location
  latitud: number
  longitud: number
  grupo: string
}

export class User {
  id: number;
  username: string;
  name: string;
  surname: string;
  password: string;
  email: string
  nameAndSurname: string
}

export class SessionInfo {
    empresa: Empresa;
    usuario: Usuario;
    permisoList: Array<string>
}
