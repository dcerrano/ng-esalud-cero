import { Subject } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

export class BaseComponent {
    
     /*Para los selects filtrables, cada vez que se modifica texto, se debe hacer una búsqueda en el backend*/
     createTextChangeListener(subjectName, listName, service) {
        let txtSubject = new Subject<string>();
        txtSubject.pipe(debounceTime(500)).pipe(distinctUntilChanged())
            .subscribe(data => {
                service.sSearch(data).subscribe(resp => {
                    this[listName] = resp;
                });
            });
        this[subjectName] = txtSubject;
        txtSubject.next('')
    }

    onTxtChanged(data, subjectName) {
        if(typeof (data )== 'string'){
            this[subjectName].next(data);
        }
    }

    
}