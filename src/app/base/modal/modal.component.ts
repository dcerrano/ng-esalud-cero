import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

    @Input() modalTitle='';
    @Input() modalId='';
    @Input() modalClass='modal-lg'

    @ViewChild('closeModalBtn') closeModalBtn: ElementRef;
    @ViewChild('showModalBtn') showModalBtn: ElementRef;
    
    constructor() { }

    ngOnInit() {
        
    }

    close() {
        this.closeModalBtn.nativeElement.click()
    }
    show() {
        this.showModalBtn.nativeElement.click()
    }
}
