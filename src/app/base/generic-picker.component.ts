
import { EventEmitter, OnInit, Output, Input, OnChanges, SimpleChanges } from '@angular/core';
import { GenericListComponent } from './generic-list.component';


export class GenericPickerComponent<T> implements OnInit, OnChanges {

    entity: T;
    @Output() entityChangedEvent: EventEmitter<T> = new EventEmitter<T>();
    @Input() required=false
    @Input() index = 0;
    @Input() pickerId :string;
    pickerModalId :string;
    
    constructor() {
        this.reset();
    }

    ngOnInit() {
        if(!this.pickerId) {
            this.pickerId = 'picker' + '-' + this.index;;
        }
        this.pickerModalId = this.pickerId + 'Modal';
        this.getListComponent().getSelectedRow().subscribe(selectedRow => {
            if (selectedRow.data) {
                this.setSelectedListItem(selectedRow.data);
                this.entityChangedEvent.emit(this.entity);
                if(selectedRow.doubleClick){
                    this.getListModal().close()
                }
            }
        });
    }

    setSelectedListItem(item) {
        this.entity = item;
    }

    getListComponent(): GenericListComponent<T> {
        return null;
    }

    getListModal() {
        return null;
    }

    setEntity(entity: T) {
        if (entity) {
            this.entity = entity;
        } else {
            this.reset();
        }
    }

    afterSetEntity(entity) {

    }
    reset() {
        this.entity = {} as T;
    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            /*let chng = changes[propName];
            let cur  = JSON.stringify(chng.currentValue);
            let prev = JSON.stringify(chng.previousValue);
            this.changeLog.push(`propName: currentValue = cur, previousValue = prev`);*/
            console.log("CHANGES", propName)
        }
        console.log("ON CHANGES")
    }
}
