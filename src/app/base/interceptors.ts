import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpSentEvent,
  HttpHeaderResponse,
  HttpProgressEvent,
  HttpResponse,
  HttpUserEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { catchError, finalize, map } from 'rxjs/operators';
import 'rxjs/add/observable/throw';
import { Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { SessionService } from './session.service';

@Injectable()
export class HTTPStatus {
  private requestInFlight$: BehaviorSubject<boolean>;
  constructor() {
    this.requestInFlight$ = new BehaviorSubject(false);
  }

  setHttpStatus(inFlight: boolean) {
    this.requestInFlight$.next(inFlight);
  }

  getHttpStatus(): Observable<boolean> {
    return this.requestInFlight$.asObservable();
  }
}

@Injectable()
export class HTTPListener implements HttpInterceptor {
  constructor(private status: HTTPStatus) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.status.setHttpStatus(true);
    return next.handle(req).pipe(
      map(event => {
        return event;
      }),
      catchError(error => {
        return Observable.throw(error);
      }),
      finalize(() => {
        this.status.setHttpStatus(false);
      })
    )
  }
}


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private sessionService: SessionService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.sessionService.getUser()) {
      return true;
    }

    // not logged in so redirect to login page with the return url and return false
    this.router.navigate(['pages/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }

}


@Injectable()
export class CustomInterceptor implements HttpInterceptor {
  intentos: number = 0;
  constructor(private injector: Injector, private router: Router ) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<
  | HttpSentEvent
  | HttpHeaderResponse
  | HttpProgressEvent
  | HttpResponse<any>
  | HttpUserEvent<any>| HttpEvent<any> > {


    request = request.clone({
      withCredentials: true
    });

    return next.handle(request).catch(error => {
      if ((error instanceof HttpErrorResponse)) {
        console.log("tipo de error en el interceptor", (<HttpErrorResponse>error).status, error);
        switch ((<HttpErrorResponse>error).status) {
          case 401:
            if(request.url.indexOf("/login")<0){
                this.router.navigate(['pages/login'], { queryParams: { returnUrl: location.pathname } });
            }
            return Observable.throw(new HttpErrorResponse({ status:401, error: 'Usuario no autenticado' }));
          case 400:
            return this.handle400Error(error); 
          case 0:
            return this.handleNoServerConnection();
          case 500:
            return this.handle500Error(error);
          case 409:
            return this.handle409Error(error);
            default:
            return Observable.throw(error);
        }
      } else {
        return Observable.throw(error);
      }
    });
  }

  handleNoServerConnection() {
    //console.log("sin conexion",!navigator.onLine);
    console.log("sin conexión en el interceptor");
    return Observable.throw(new HttpErrorResponse({ error: 'Verifique su conexión e intente de nuevo.' }));
  }

  handle500Error(resp) {
    let msg = null
    if (resp.error.errorMessage) {
      msg = resp.error.errorMessage
    } else if(resp.error.msg) {
      msg = resp.error.msg
    } else {
      msg = 'Problemas en el servidor.'
    }
    return Observable.throw(new HttpErrorResponse({ error:msg }));
  }

  handle409Error(resp, status?) {
    console.error("interceptor", resp)
    let msg = null
  
    if (resp.error.errorMessage) {
      msg = resp.error.errorMessage
    } else if(resp.error.msg) {
      msg = resp.error.msg
    } else {
      msg = 'Problemas en el servidor.'
    }
    return Observable.throw(new HttpErrorResponse({ error: msg, status: 409 }));
  }


  handle400Error(resp) {
   
    let fieldErrors=[]
    if (resp.error && resp.error.fieldErrors) {
       fieldErrors=resp.error.fieldErrors
    }
    return Observable.throw(new HttpErrorResponse({ error: {msg:'Errores de validación.',  fieldErrors:fieldErrors}, status:400 }));
  }
}
