import { OnInit } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { FormAction, GenericFormComponent } from "./generic-form.component";
import { GenericListComponent } from "./generic-list.component";

export class GenericListFormComponent<T> implements OnInit {
    list: GenericListComponent<T>;
    form: GenericFormComponent<T>;
    hiddenTabs = {};
    selectedTab = 1;
    selectedEntity: any;
    /*Algunos pickers pueden permitir crear primero el registro y seleccionar*/
    entityForPickerSubject: BehaviorSubject<T> = new BehaviorSubject(null);

    isTabHidden(tab){
        return this.hiddenTabs[tab];
    }
    
    hideTab(tab) {
       this.hiddenTabs[tab] = true;
    }

    showTab(tab) {
        this.hiddenTabs[tab] = false;
    }
    

    setFormEntity(entity: T) {
        this.form.setEntity(entity);
        this.setSelectedTab(2);
    }

    ngOnInit(): void {
        this.list.getSelectedRow().subscribe(selectedDTRow => {
            if (selectedDTRow.data) {
                this.onSelectedRow(selectedDTRow);
            }
        })

        this.list.getNewRecordSubject().subscribe(resp => {
            this.onNewRecord();
        });

        if (this.form){
            this.form.getEntityChangedSubject().subscribe(formAction => {
                this.onFormEntityChanged(formAction);
            });
        }
    }

    onSelectedRow(selectedDTRow) {
        this.selectedEntity = selectedDTRow.data;
        if (selectedDTRow.doubleClick) {
            this.entityForPickerSubject.next(this.selectedEntity);
        } else {
            this.setFormEntity(this.selectedEntity)
        }
    }

    onNewRecord() {
        const that = this;
        this.setSelectedTab(2);
        this.form.abmButtonsCmp.onNewButtonClicked();
        /*primero cambiar de pestaña y luego ejecutar focus()*/
        setTimeout(function () {
            that.form.focus();
        }, 1000);

    }

    onFormEntityChanged(formAction: FormAction) {
        this.selectedEntity = formAction.entity;
        /*Actualizar lista solo si se modifica o borra registro*/
        if (formAction.isModifiedOrDeletedEntity()) {
            this.list.rerender();
        }
        if (formAction.isModifiedEntity()) {
            this.entityForPickerSubject.next(this.selectedEntity);
        }
        this.onFormAction(formAction);
    }

    onFormAction(formAction: FormAction) {

    }

    public getEntityForPickerSubject(): Observable<T> {
        return this.entityForPickerSubject.asObservable();
    }

    isTabSelected(tab) {
        return this.selectedTab == tab;
    }
    setSelectedTab(tab) {
        this.selectedTab = tab;
        if (tab==2 && this.form) {
            this.form.markAsTouched()
        }
    }
}
