
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import "rxjs/add/observable/throw";
import "rxjs/add/operator/catch";
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Global } from '../Global';
import { SessionInfo, Empresa } from 'app/models/common.models';


@Injectable()
export class SessionService {
  user: any;
  sessionInfo: SessionInfo = new SessionInfo();
  subjectSessionInfo: BehaviorSubject<SessionInfo> = new BehaviorSubject(new SessionInfo());

  constructor(private http: HttpClient) {

  }

  setUser(user) {
    this.user = user;
  }

  getUser() {
    return this.user;
  }

  getSessionInfo() {
    return this.sessionInfo;
  }

  public getSubjectSessionInfo(): Observable<SessionInfo> {
    return this.subjectSessionInfo.asObservable();
  }

  setSessionInfo(sessionInfo) {
    this.sessionInfo = sessionInfo;
    this.sessionInfo.empresa = new Empresa()
    this.sessionInfo.empresa.codigo = 'MAN'
    this.sessionInfo.empresa.razonSocial = 'MANTICS S.A.'
    this.user = sessionInfo.usuario
      this.subjectSessionInfo.next(sessionInfo);
  }



  public checkSession(): Observable<SessionInfo> {
    return this.http.get<SessionInfo>(Global.SESSION_INFO_URL);
  }

  public login(credentials): Observable<SessionInfo> {
    if (credentials.username === null || credentials.password === null) {
      return Observable.throw("Por favor ingrese sus credenciales");
    } else {
      let formData: FormData = new FormData();
      formData.append('username', credentials.username);
      formData.append('password', credentials.password);
      return this.http.post<SessionInfo>(Global.LOGIN_URL, formData);
    }
  }


  public logout(): Observable<any> {
    return this.http.post(Global.LOGOUT_URL, '');
  }

  confirmPass(password, token) {
    var fd = new FormData();
    fd.append('password', password);
    fd.append('token', token);
    return this.http.post<any>(Global.URL_BASE + "resetpass/confirm", fd);
  }


  hasPermission(permissionName, ignoreRoot?) {
    const permisoList = this.sessionInfo.permisoList

    if (!permisoList) {
      return false
    }

    if (ignoreRoot) {
      return permisoList.indexOf(permissionName) >= 0
    }

    return permisoList.indexOf('root') >= 0 || permisoList.indexOf(permissionName) >= 0
  }

  changePass(currentPass, newPass, confirmPass) {
    let params =  {currentPass: currentPass, newPass: newPass, confirmPass: confirmPass}
     return this.http.post<any>(Global.URL_BASE + "mi-sesion/cambiar-pass?" + $.param(params), null);
  }

}

