import swal from "sweetalert2";
import * as jsPDF from "jspdf";
import { Global } from "app/Global";

export class htroot {

    static lenguaje =
        {
            "emptyTable": "No se encontró ningún registro",
            "info": "Mostrando _START_ hasta _END_ de _TOTAL_ resultados",
            "infoEmpty": "No se encontró ningún registro",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ resultados",
            "loadingRecords": "Cargando registros...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "No se encontró ningún registro",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        };

    static isBlank(value) {
        if (value == null) {
            return true;
        }
        if (typeof value === 'string') {
            return value == "null" || value.trim() == '';
        }
        return false;
    }

    static strEsToDate = function (dateStr) {

        if (!dateStr) {
            return null;
        }

        if (typeof (dateStr) == 'object') {
            return dateStr;
        }

        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0])

    }

    static pad = function (pad, str, padLeft) {
        if (typeof str === 'undefined')
            return pad;
        if (padLeft) {
            return (pad + str).slice(-pad.length);
        } else {
            return (str + pad).substring(0, pad.length);
        }
    }

    static pad0(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

    static removeListItem(list, item) {
        var index = list.indexOf(item);
        if (index > -1) {
            list.splice(index, 1);
        }
    }

    static toCurrency(num) {
        if (!num) {
            return '0'
        }
        return num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
    }
    static dotted(num) {
        return num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
    }
    static money(number, fill?, maxlength?) {
        let res = this.toCurrency(number);
        if (!maxlength) {
            maxlength = 12
        }
        let len = res.length;

        let pad = ''
        let count = (maxlength - len) - len / 4
        for (let i = 1; i <= count; i++) {
            pad += ' '
        }

        return pad + res;
    }

    static toMoney(doc, value, x, y) {
        if (value && value.toFixed) {
            value = value.toFixed(0)
        }

        doc.text(this.toCurrency(value), x, y, 'right');
    }

    static toTxt(doc,value, x, y) {
       if (value) {
           doc.text(x , y , value)
       }
    }

    static toDec2(doc, value, x, y) {
        if (value && value.toFixed) {
            value = value.toFixed(2)
        }

        doc.text(this.toCurrency(value), x, y, 'right');
    }

    static getErrorMessage(response) {

        if (response.status < 0) {
            return 'No se pudo establecer conexión con el servidor';
        }

        if (response.status === 500) {
            return 'Ocurrió un error en el servidor';
        }

        if (response.status === 400 && response.error.errorMessage == null) {
            return 'Errores de validación';
        }

        if (response.status === 401) {
            return 'Usuario o contraseña incorrectos';
        }

        if (response.status === 403) {
            return 'No tiene permiso para este proceso';
        }

        if (response.status === 409) {
            if (response.error && typeof response.error === 'string') {
                return response.error
            }
            if(response.error.errorMessage){
                return response.error.errorMessage;
            }
        }

        if (response == null || response.error == null || response.error.errorMessage == null) {
            return 'Server Error';
        }

        return response.error.errorMessage;
    };

   static getHoras(desde?, hasta?) {
        if(!desde) {
            desde=0
        }
        if(!hasta) {
            hasta=23
        }
        let horas = [];
        for (let i = desde; i <= hasta; i++) {
            if (i < 10) {
                horas.push('0' + i);
            } else {
                horas.push(i);
            }
        }
        return horas;
    }



    static htmlSanitize(str) {
        var entityMap = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': '&quot;',
            "'": '&#39;',
            "/": '&#x2F;'
        };
        return String(str).replace(/[&<>"'\/]/g, function (s) {
            return entityMap[s];
        });
    }

    static isEmailOk(text) {
        const validEmailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return validEmailRegEx.test(text);
    }

    static meses = [{ id: 1, nombre: 'ENERO' }, { id: 2, nombre: 'FEBRERO' }, { id: 3, nombre: 'MARZO' }, { id: 4, nombre: 'ABRIL' },
    { id: 5, nombre: 'MAYO' }, { id: 6, nombre: 'JUNIO' }, { id: 7, nombre: 'JULIO' }, { id: 8, nombre: 'AGOSTO' }, { id: 9, nombre: 'SETIEMBRE' },
    { id: 10, nombre: 'OCTUBRE' }, { id: 11, nombre: 'NOVIEMBRE' }, { id: 12, nombre: 'DICIEMBRE' },]

    static mesesAbr = [{ id: 1, nombre: 'ENE' }, { id: 2, nombre: 'FEB' }, { id: 3, nombre: 'MAR' }, { id: 4, nombre: 'ABR' },
    { id: 5, nombre: 'MAY' }, { id: 6, nombre: 'JUN' }, { id: 7, nombre: 'JUL' }, { id: 8, nombre: 'AGO' }, { id: 9, nombre: 'SET' },
    { id: 10, nombre: 'OCT' }, { id: 11, nombre: 'NOV' }, { id: 12, nombre: 'DIC' },]

    static getFechaStr(fecha: Date) {
        return fecha.getDate() + ' de ' + this.meses[fecha.getMonth()].nombre + ' de ' + fecha.getFullYear()
    }
    static showInfoPopup(title, text) {
        swal({
            title: title,
            text: text,
            type: 'success',
            confirmButtonClass: "btn btn-primary",
            buttonsStyling: false
        }).catch(swal.noop)
    }

    static showErrorPopup(title, text) {
        swal({
            title: title,
            text: text,
            type: 'error',
            confirmButtonClass: "btn btn-error",
            buttonsStyling: false
        }).catch(swal.noop)
    }

    static showConfirmPopup(title, text) {
        return swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sí, confirmar',
            cancelButtonText: 'No, cancelar',
            confirmButtonClass: "btn btn-primary",
            cancelButtonClass: "btn btn-danger",
            buttonsStyling: false
        })
    }

    static getFirmaReciboImg(empresa, callback) {
        let firmaUrl = "assets/img/" + empresa.codigo + "_firma_recibo.png"
        htroot.imgToBase64(
            firmaUrl,
            firmaImg => {
                callback(firmaImg)
            }
        );
    }

    static getLogoImg(callback) {
        const url = Global.URL_BASE + "mi-empresa/image/0.png";
        htroot.imgToBase64(
            url,
            logo => {
                callback(logo)
            }
        );
    }

    static getLogoAndFirmaImg(empresa, callback) {
        this.getLogoImg(logo=>{
            this.getFirmaReciboImg(empresa, firma=>{
                callback(logo, firma)
            })
        })
    }

    static imgToBase64(url, callback) {
        if (!window['FileReader']) {
            callback(null);
            return;
        }
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true
        xhr.responseType = 'blob';
        xhr.onload = function () {
            var reader = new FileReader();
            reader.onloadend = function () {

                const str = (reader.result as string).replace('text/html', 'image/png')
                callback(str, xhr.getResponseHeader('fechahora'));
            };
            reader.readAsDataURL(xhr.response);
        };

        xhr.open('GET', url);
        xhr.send(null);
    };

    static getJsPdf(title, subject, orientation?: string): jsPDF {
        let prop = {
            title: title,
            subject: subject,
            author: "MANTICS",
            creator: "MANTICS"
        };
        let doc = new jsPDF(orientation || "p", "mm", "a4");
        doc.setProperties(prop)
        return doc
    }

    static toText(text) {
        if (!text) {
            return '';
        }
        return text.normalize('NFD')
            .replace(/([aeio])\u0301|(u)[\u0301\u0308]/gi, "$1$2")
            .normalize();
    }

    static openPdf(doc, filename) {

        if (navigator.userAgent.indexOf("Chrome") != -1 && navigator.userAgent.indexOf("SamsungBrowser") ==-1) {
            doc.save(filename);

        } else {
            doc.output('dataurlnewwindow');
        }
    };
}
