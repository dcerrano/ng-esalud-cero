import { Global } from "./../Global";
import * as jsPDF from "jspdf";
import { htroot } from "app/base/htroot";
import { SessionInfo } from "app/models/common.models";


export class GenericReport {
    txt = htroot.toText;
    sessionInfo: SessionInfo;
    doc: jsPDF;
    filename = "";
    title1 = "";
    title2 = "";
    title3 = "";
    logo: any;
    xlength = 285;
    startY = 35;
    landscape = true;
    list = [];
    fechaHora: string;
    groupMap = {};

    portrait() {
        this.landscape = false;
        this.xlength = 200;
    }

    setText(x, y, text, maxlength, fontSize) {
        var textlen = text.length * 1.7; //letras son más grandes
        if (text && textlen > maxlength) {
            this.doc.setFontSize(fontSize - 1);
        }
        if (text && textlen > maxlength * 1.1) {
            text = text.substring(0, maxlength / 1.7) + "...";
        }

        this.doc.text(x, y, text);
    }

    getFileName(suffix) {
        var title = this.sessionInfo.empresa.codigo;
        title += "_" + suffix;
        return title + ".pdf";
    }

    createReportHeader(y) {
        this.doc.setDrawColor(160, 160, 160);
        this.doc.line(15, y, this.xlength, y);
        this.doc.line(15, y, 15, y + 17); //barra vertical izq
        this.doc.line(this.xlength, y, this.xlength, y + 17); //barra vertical derecha
        let emp = this.sessionInfo.empresa
        this.doc.addImage(this.logo, "JPEG", 16, y + 1, emp.logoWidth, emp.logoHeight);
        this.doc.setFontType("bold");
        this.doc.setFontSize(10);
        this.addTitle(this.title1, y + 5);
        this.doc.setFontSize(9);
        this.addTitle(this.title2, y + 9);
        this.doc.setFontSize(8);
        this.addTitle(this.title3, y + 13);
        this.doc.line(15, y + 17, this.xlength, y + 17);
    }

    addTitle(title, y) {
        if (!title) {
            return;
        }
        let emp = this.sessionInfo.empresa
        let centerX = this.xlength / 2 + emp.logoWidth / 2;
        let maxSize = this.xlength - emp.logoWidth - 25; //25 es por los bordes
        let splitText = this.doc.splitTextToSize(title, maxSize);
        this.doc.text(splitText, centerX, y, "center");
    }

    getEmpresaDescrip() {
        let empresa = this.sessionInfo.empresa;
        let empresaInfo = empresa.razonSocial;
        if (empresaInfo != empresa.empleador) {
            empresaInfo += " - " + empresa.empleador;
        }
        return empresaInfo;
    }

    createOrAddMapItem(map, key, item) {
        if (map[key]) {
            map[key].items.push(item);
        } else {
            map[key] = { items: [item] };
        }
    }

    createPdf() {
        const url = Global.URL_BASE + "mi-empresa/image/0.png";
        htroot.imgToBase64(url, (logo, fechaHora) => {
            this.logo = logo;
            if (fechaHora) {
                this.fechaHora = fechaHora;
            }
            this.createContent();
        });
    }
    
    createHeader() {}

    
    getGroupBy() {
        return "areatrabajo";
    }

    group() {
        this.groupMap = {};
        this.list.forEach(item => {
            this.createOrAddMapItem(this.groupMap, item[this.getGroupBy()], item);
        });
    }

    onInitGroupForEach(groupBy, index, list, y) {
        return y;
    }

    getGroupList() {
       return Object.keys(this.groupMap).sort();   
    }

    getItemListByGroup(groupBy) {
       return  this.groupMap[groupBy].items
    }
    
    createContent() {
        let doc = this.doc;
        this.createHeader();
        let y = this.startY;
        this.group();
        let groupList = this.getGroupList();
        let idxGlobal = 1;
        let sumGlobal = this.createSum();
        groupList.forEach((groupBy, index) => {
            let list = this.getItemListByGroup(groupBy)
            y = this.onInitGroupForEach(groupBy, index, list, y);
            let itemCount = list.length;
            let lastItem = list[itemCount - 1];
            y = this.checkGroupPageBreak(y, list);
            let sumArea = this.createSum();
            let idxGroup = 1;
            list.forEach(item => {
                item.count = 1;
                doc.setFontType("normal");
                let newY = this.createRow(y, item, idxGlobal++, idxGroup++);
                if (newY) {
                    y = newY;
                }
                this.sum(sumGlobal, item);
                this.sum(sumArea, item);
                y = y + 5;
                if (item != lastItem) {
                    //si es el último registro y justo y>280, entonces evitar que footer pase a siguiente hoja
                    y = this.checkPageBreak(y);
                }
            });
            let newY = this.createGroupFooter(y, groupBy, sumArea, false, list);
            if (newY) {
                y = newY;
            } else {
                y = y + 10;
            }

            y = this.checkPageBreak(y);
        });
        doc.line(15, y - 4, this.xlength, y - 4);
        this.createGroupFooter(y, "TOTALES", sumGlobal, true, this.list);
        y = y + 10;
        this.doc.setFontType("normal");
        this.addPageNumbers();
        htroot.openPdf(doc, this.filename);
    }

    createRow(y, item, idxGlobal, idxGroup?) {
        return y;
    }

    createAreaFooter(y, area, sum) {
        this.createGroupFooter(y, area, sum);
    }

    createGroupFooter(y, group, sum, global?, list?) {
        let doc = this.doc;
        // doc.line(15, y - 4, this.xlength, y - 4);
        doc.setFontType("bold");
        group = group + " (" + sum.count + ")";
        doc.text(20, y, group);
        doc.line(15, y + 1, this.xlength, y + 1);
        return y + 10;
    }

    createSum() {
        return { count: 0 };
    }

    addPageNumbers(x?, y?) {
        if (!x) {
            if (this.landscape) {
                x = 270;
                y = 200;
            } else {
                x = 175;
                y = 285;
            }
        }
        let totalPages = this.doc.internal.getNumberOfPages();
        let currentPage = totalPages;
        if (!this.fechaHora) {
            let now = new Date();
            this.fechaHora = now.toLocaleDateString("es") + " " + now.toLocaleTimeString("es");
        }

        while (currentPage > 0) {
            this.doc.setPage(currentPage);
            this.doc.text(x, y, "Página " + currentPage + " de " + totalPages);
            this.doc.text(20, y, this.fechaHora);
            currentPage--;
        }
    }

    checkPageBreak(y) {
        let maxY = this.getMaxY();
        if (y > maxY) {
            this.doc.setFontType("normal");
            this.doc.addPage();
            this.createHeader();
            y = this.startY;
        }
        return y;
    }

    getMaxY(): number {
        return this.landscape ? 190 : 275;
    }

    checkAreaPageBreak(y, items?) {
        return this.checkGroupPageBreak(y, items);
    }

    checkGroupPageBreak(y, items?) {
        let maxY = this.getMaxY();
        let topY = maxY + 5;
        if (y >= maxY) {
            y = topY;
            return this.checkPageBreak(y);
        } else if (items && y + items.length > 1 && y + 10 > maxY) {
            y = topY;
            return this.checkPageBreak(y);
        }
        return y;
    }

    sum(sumGlobalOrArea, item) {
        if (this.ignoredForSum(item)) {
            return 
        }
        Object.keys(sumGlobalOrArea).forEach(key => {
            if (item[key]) {
                sumGlobalOrArea[key] += item[key];
            }
        });
    }

    ignoredForSum(item) {
        return false
    }


    text(x, y, value, maxChars?, maxLines?) {
        if (!value) {
            return
        }
        maxLines = maxLines? maxLines : 2
        maxChars = maxChars? maxChars : 50
    
        let splitText = this.doc.splitTextToSize(value, maxChars);
        let arrayLen = splitText.length
        arrayLen = arrayLen > maxLines? maxLines : arrayLen
        splitText = splitText.slice(0, arrayLen)
    
        this.doc.text(x, y, splitText)
        return (arrayLen -1) * 3 +  y
    }

}
