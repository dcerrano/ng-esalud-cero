import { EventEmitter } from '@angular/core';
import { Component, Input, SimpleChanges, Output } from '@angular/core';


@Component({
  selector: 'app-toogle-checkbox',
  templateUrl: './toogle-checkbox.component.html'
})
export class ToogleCheckboxComponent {
  @Input() value = false
  @Input() caption: string
  @Output() valueChangedEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  onChangeValue(value) {
    this.valueChangedEvent.emit(value);
  }
}
