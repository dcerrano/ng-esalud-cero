import { Component } from '@angular/core';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { delay } from 'rxjs/operators';
import swal from 'sweetalert2';
import { GenericFormComponent } from '../generic-form.component';
import { HTTPStatus } from '../interceptors';

@Component({
  selector: 'app-abm-buttons',
  templateUrl: './abm-buttons.component.html'
})
export class AbmButtonsComponent<T> {
  public HTTPActivity: boolean;

  public saveBtnEnabled = true;
  public newBtnEnabled = false;
  public deleteBtnEnabled = false;
  form: GenericFormComponent<T>;
  myFormVar: NgForm;

  constructor(private httpStatus: HTTPStatus) {
    this.httpStatus.getHttpStatus().pipe(delay(100)).subscribe((status: boolean) => { this.HTTPActivity = status });
  }

  setForm(form) {
    this.form = form;
    this.myFormVar = form.myFormVar;
  }

  onNewButtonClicked() {
    this.saveBtnEnabled = true;
    this.newBtnEnabled = false;
    this.deleteBtnEnabled = false;
    this.form.onNewRecord();
  }

  onSaveButtonClicked() {
    this.form.onSaveRecord();
  }

  getConfirmDeleteMsg() {
    return "Desea borrar registro de forma permanente?";
  }

  onDeleteButtonClicked(entityId) {
    swal({
      title: 'Borrado de registro',
      text: this.getConfirmDeleteMsg(),
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, deseo borrar',
      cancelButtonText: 'No, cancelar',
      confirmButtonClass: "btn btn-primary",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.form.onConfirmDelete(entityId);
      }
    })
  }
}
