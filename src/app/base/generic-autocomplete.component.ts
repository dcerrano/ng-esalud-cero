import { Subject } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { GenericService } from "./generic.service";
import { Input, Output, EventEmitter, OnInit, ViewChild, ElementRef, SimpleChanges, OnChanges } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ModalComponent } from "./modal/modal.component";

export class GenericAutoCompleteComponent<T> implements OnInit, OnChanges {
    @ViewChild("autocompleteInput") autocompleteInput: ElementRef
    @Input() index = 0;
    @Input() entity: T;
    @Input() placeholder = '';
    @Input() required = false;
    @Input() disabled = false
    @Input() addButtonDisabled = false;
    @Input() focusInput = false;
    @Output() entityChangedEvent: EventEmitter<T> = new EventEmitter<T>();
    @ViewChild('myAutoCompleteFormVar') myAutoCompleteFormVar: NgForm;

    formModalId: string;
    list: T[] = [];
    listName: 'list'
    constructor(private baseName: string, public service?: GenericService<T>) {

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.entity) {
            this.onEntityChange(changes.entity)
        }
    }

    onEntityChange(entity) {

    }

    ngOnInit() {
        let id = '-' + this.index;
        this.formModalId = this.baseName + 'FormModal' + id;
        this.createTextChangeListener(this.baseName + 'Subject', 'list');
        this.createModalListeners();
    }

    markAsTouched() {
        if (this.myAutoCompleteFormVar) {
            for (const field in this.myAutoCompleteFormVar.controls) {
                this.myAutoCompleteFormVar.controls[field].markAsTouched()
            }
        }
    }

    /*Para los selects filtrables, cada vez que se modifica texto, se debe hacer una búsqueda en el backend*/
    createTextChangeListener(subjectName, listName) {
        let txtSubject = new Subject<string>();
        txtSubject.pipe(debounceTime(500)).pipe(distinctUntilChanged())
            .subscribe(sSearch => {
                if(!sSearch){
                    sSearch=''
                }
                this.serverSearch(listName, sSearch)
            });
        this[subjectName] = txtSubject;
        txtSubject.next('')
    }

    appendSsearch(sSearch) {
        return sSearch
    }

    serverSearch(listName, sSearch) {
        sSearch = this.appendSsearch(sSearch)
        this.appendSsearch(sSearch)
        this.service.sSearch(sSearch).subscribe(resp => {
            this.onGetServerList(resp)
        });
    }

   onGetServerList(list) {
        this['list'] = list;
        this.selectFirst()
    }

    selectFirst(){
        let list = this['list']
        if (list.length == 1) {
            this.entity = list[0]
            this.emit(this.entity)
        }
        if (this.focusInput) {
            this.focus()
        }
    }

    createModalListeners() {
        let form = this.getForm();
        if (form) {
            form.getEntityChangedSubject().subscribe(formAction => {
                if (formAction.isModifiedEntity()) {
                    this.entity = formAction.entity as T;
                    this.entityChangedEvent.emit(this.entity);
                    this.getFormModal().close();
                }
            });
        }
    }

    getForm() {
        return null;
    }

    getFormModal(): ModalComponent {
        return null;
    }

    onTxtChanged(data, subjectName) {
        /*Si es string, se está buscando registro. Si es objeto, se seleccionó de la lista */
        if (typeof (data) == 'string') {
            this[subjectName].next(data);
            /*Si se borra todo el texto de la búsqueda, considerar que campo no tendrá valor*/
            if (data == '') {
                this.entityChangedEvent.emit(null);
                this.entity = null;
            }
        } else {
            this.emit(data)
        }
    }

    emit(data) {
        this.beforeEmit(data)
        this.entityChangedEvent.emit(data);
    }

    beforeEmit(data) {
  
    }

    focus() {
        this.autocompleteInput.nativeElement.focus()
    }

    displayFn(obj: any) {
        return obj ? obj.str : ''
    }

    onClickAddButton(event?) {
        let modal = this.getFormModal()
        if (modal) {
            modal.show()
            let form = this.getForm()
            if (form) {
                /*primero mostrar modal y luego ejecutar focus()*/
                setTimeout(function () {
                    form.focus()
                }, 1000);
            }
        }
        if(event) {
            event.stopPropagation()
        }
    }
}
