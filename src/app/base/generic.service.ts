
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Global } from '../Global';
import { DataTablesResponse } from './generic-list.component';


export class GenericService<T> {
    protected url: string;

    constructor(protected http: HttpClient, mapping: string) {
        this.url = Global.URL_BASE + mapping;
    }

    findOne(id) {
        return this.http.get<T>(this.url + '/' + id);
    }

    getList() {
        return this.http.get<T[]>(this.url);
    }

    suggest() {
        return this.http.get<Array<T>>(this.url + '/suggest?iDisplayLength=20');
    }

    sSearch(s) {
        if (!s) {
            s = '';
        }
        return this.http.get<Array<T>>(this.url + '/suggest?iDisplayLength=20&sSearch=' + s);
    }

    save(fd) {
        return this.http.post(this.url, fd);
    }

    saveList(params) {

        return this.http.post(this.url + '/savelist', params, {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8')
        });
    }

    delete(id) {
        return this.http.delete(this.url + '/' + id);
    }

    dataTable(dataTablesParameters) {
        return this.http
            .get<DataTablesResponse>(this.url + '/datatable?' + $.param(dataTablesParameters))

    }

    getImageUrl(id) {
        return this.url + '/image/' + id + ".png?now=" + new Date();
    }

    getImagePath(entity) {
        let url = this.url + '/image/0.png'
        if (entity.updHash) {
            url = this.url + '/image/' + entity.id + ".png?updHash=" + entity.updHash
        } else {
            url = this.url + '/image/' + entity.id + ".png?now=" + new Date();
        }
        return url
    }
}