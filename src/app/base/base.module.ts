import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AbmButtonsComponent } from './abm-buttons/abm-buttons.components';
import { FormNotificationsComponent } from './form-notifications/form-notifications.component';
import { ModalComponent } from './modal/modal.component';
import { FormsModule } from '@angular/forms';
import { ToogleCheckboxComponent } from './toogle-checkbox/toogle-checkbox.components';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
@NgModule({
    imports: [ RouterModule, CommonModule, FormsModule ],
    declarations: [ AbmButtonsComponent ,FormNotificationsComponent, ModalComponent, 
 ToogleCheckboxComponent, ConfirmDialogComponent],
    exports: [ AbmButtonsComponent, FormNotificationsComponent, ModalComponent, 
      ToogleCheckboxComponent, ConfirmDialogComponent],
    entryComponents: [ConfirmDialogComponent]
})


export class BaseModule {}
