import { OnInit, ViewChild, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BehaviorSubject, Observable } from "rxjs";
import swal from "sweetalert2";
import { AbmButtonsComponent } from "./abm-buttons/abm-buttons.components";
import { BaseComponent } from "./base.component";
import { FormNotificationsComponent } from "./form-notifications/form-notifications.component";
import { GenericService } from "./generic.service";
import { htroot } from "./htroot";

export class FormAction {
    action: string
    entity: Object

    constructor(action: string, entity: Object) {
        this.action = action
        this.entity = entity
    }

    isModifiedOrDeletedEntity() {
        return this.action == 'SAVE' || this.action == 'DELETE'
    }

    isModifiedEntity() {
        return this.action == 'SAVE'
    }

    isDeletedEntity() {
        return this.action == 'DELETE'
    }
    isNewEntity() {
        return this.action == 'NEW'
    }
}

export class GenericFormComponent<T> extends BaseComponent implements OnInit {

    @ViewChild(FormNotificationsComponent) protected formNotifCmp: FormNotificationsComponent;
    @ViewChild(AbmButtonsComponent) public abmButtonsCmp: AbmButtonsComponent<T>;
    public entity: T;
    lastEntity: T;
    public multipartFile: any;
    public multipartFilePreviewSrc;
    clearMultipartFileBtn: HTMLElement;
    entityChangedSubject: BehaviorSubject<FormAction> = new BehaviorSubject(new FormAction(null, null));
    @ViewChild('myFormVar') myFormVar: NgForm;
    @ViewChild('focusInput') focusInput: ElementRef;
    currencyOptions = { prefix: '', thousands: '.', decimal: ',', precision: 0 };
    float1Options = { prefix: '', thousands: '.', decimal: ',', precision: 1 };
    tab = 1

    constructor(public service: GenericService<T>) {
        super();
    }

    ngOnInit(): void {
        this.abmButtonsCmp.setForm(this);
        this.abmButtonsCmp.onNewButtonClicked();
        this.clearMultipartFileBtn = document.getElementById('multipartFileBtn');
        this.markAsTouched()
    }

    onNewRecord() {
        this.entity = this.newEntity();
        this.focus();
        this.multipartFile = null;
        this.formNotifCmp.showOnlyInfo("Complete los campos");
        this.entityChangedSubject.next(new FormAction('NEW', this.entity));
        this.markAsTouched()
        this.afterNewRecord();
    }

    markAsTouched() {
        for (const field in this.myFormVar.controls) {
            this.myFormVar.controls[field].markAsTouched()
        }
    }

    newEntity(): T {
        this.tab = 1
        return {} as T
    }

    afterNewRecord() {
        this.showImage(this.entity);
    }

    beforeAppendFormData(fd) {

    }

    appendFormData(fd) {
        this.beforeAppendFormData(fd);
        this.objectToFormData(this.entity, fd, null, 0);
        if (this.multipartFile) {
            fd.append("multipartFile", this.multipartFile);
        } else {
            delete fd["multipartFile"];
        }

        this.afterAppendFormData(fd);
    };

    afterAppendFormData(fd) {

    }

    objectToFormData(obj, formData, prefix, stack?: number) {
        if (stack > 10000) {
            alert("Stack overflow. GenericFormComponet.objectToFormData")
            return
        }
        if (!prefix) {
            prefix = '';
        } else {
            prefix = prefix + '.';
        }
        let excludedProperties = this.getExcludedProperties()
        for (var property in obj) {
            if (excludedProperties.indexOf(prefix + property) >= 0) {
                continue;
            }
            var value = obj[property];
            if (!htroot.isBlank(value)) {
                if (value instanceof Date) {
                    formData.append(prefix + property, value.toLocaleDateString('es'));
                } else if (value instanceof Object) {
                    this.objectToFormData(value, formData, prefix + property, stack++);
                    localStorage.setItem(property, JSON.stringify(value));
                } else {
                    formData.append(prefix + property, value);
                }
            }
        }
    };

    getExcludedProperties() {
        return []
    }

    onSaveRecord() {
        console.log("Guardando registro:", this.entity);
        var fd = new FormData();
        this.appendFormData(fd);
        this.serviceSave(fd);
    };

    serviceSave(fd) {
        this.service.save(fd).subscribe(resp => {
            this.onSaveSuccess(resp);
        }, resp => {
            this.onSaveError(resp);
        });
    }

    onSaveSuccess(response) {
        console.log("onSaveSuccess:", response);
        this.patchValue(response);
        //TODO: HttpResponse
        if (response.status === 201) {
            this.formNotifCmp.showOnlyInfo('Registro creado con éxito');
        } else {
            this.formNotifCmp.showOnlyInfo('Registro guardado con éxito');
        }

        this.abmButtonsCmp.newBtnEnabled = true;
        this.abmButtonsCmp.deleteBtnEnabled = true;
        this.entityChangedSubject.next(new FormAction('SAVE', this.entity));
        this.afterSaveSuccess();
    };

    public getEntityChangedSubject(): Observable<FormAction> {
        return this.entityChangedSubject.asObservable();
    }

    patchValue(entity) {
        this.entity = entity;
        this.showImage(entity);
    }

    beforeSetEntity(entity) {
        return entity;
    }


    setEntity(entity) {
        console.log("SET ENTITY:", entity);
        this.clearMultipartFile();
        entity = this.beforeSetEntity(entity);
        this.patchValue(entity);
        this.abmButtonsCmp.newBtnEnabled = true;
        this.abmButtonsCmp.deleteBtnEnabled = true;
        this.abmButtonsCmp.saveBtnEnabled = true;
        this.formNotifCmp.showOnlyInfo('Editando registro...');
        this.entity = this.afterSetEntity(entity);
        this.focus();
    }

    afterSetEntity(entity) {

        return entity;
    }

    afterSaveSuccess() {
        this.lastEntity = this.entity
    };

    onSaveError(response) {
        console.error("onSaveError:", response);
        this.formNotifCmp.showSaveError(response);
    };

    onConfirmDelete(entityId) {
        if (entityId == null) {
            entityId = this.entity['id'];
        }
        this.service.delete(entityId).subscribe(resp => { this.onDeleteSuccess(resp) }, resp => { this.onDeleteError(resp) });
    };

    onDeleteSuccess(response) {

        this.formNotifCmp.showOnlyInfo('El registro ha sido borrado!');
        swal({
            title: 'Borrado!',
            text: 'El registro ha sido borrado.',
            type: 'success',
            confirmButtonClass: "btn btn-primary",
            buttonsStyling: false
        }).catch(swal.noop)

        this.abmButtonsCmp.deleteBtnEnabled = true;
        this.entityChangedSubject.next(new FormAction('DELETE', this.entity));
        this.abmButtonsCmp.onNewButtonClicked();
    }

    onDeleteError(response) {
        var error = htroot.getErrorMessage(response);

        swal({
            title: 'Error',
            text: 'No se pudo borrar registro. ' + error,
            type: 'error',
            confirmButtonClass: "btn btn-error",
            buttonsStyling: false
        }).catch(swal.noop)
    }

    compareEntityFn(item, selected) {
        if (selected) {
            return item.id == selected.id;
        }
        return false;
    }

    onDateValueChange(event) {
        /* let value = event.targetElement.value;
         let name = event.targetElement.name;
         this.entity[name] = value;*/
    }

  selectFile(event) {
    const reader: any = new FileReader();
    const that = this;
    reader.onload = function(e) {
      $("#multipartFilePreview")
        .attr("src", e.target.result)
        .fadeIn("slow");
      that.multipartFilePreviewSrc = e.target.result;
    };

    this.multipartFile = event.target.files[0];
    reader.readAsDataURL(event.target.files[0]);
  }

  clearMultipartFile() {
    if (this.clearMultipartFileBtn) {
      this.clearMultipartFileBtn.click();
    }
    this.multipartFile = null;
    this.multipartFilePreviewSrc = this.getImagePreview();
  }

    getImagePreview() {
        return './assets/img/placeholder.jpg';
    }

  showImage(entity) {
    this.clearMultipartFile();
    let id = entity.id;
    if (id) {
      this.multipartFilePreviewSrc = this.service.getImageUrl(id);
    }
  }

  focus() {
    if (this.focusInput) {
      setTimeout(() => {
        this.focusInput.nativeElement.focus();
      }, 700);
    }
  }

  onValueChanged(fieldName, value) {
    this.entity[fieldName] = value
  }
  //mismo que onValueChanged, pero para unificar
  setFieldValue(fieldName, value) {
    this.entity[fieldName] = value
  }

  setSelectedTab(tab) {
      this.tab = tab
  }

  isTabSelected(tab) {
      return this.tab == tab
  }
}
