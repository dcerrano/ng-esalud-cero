import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html'
})
export class ConfirmDialogComponent {
  title = 'CONFIRMAR'
  message = ''
  constructor(
              public dialogRef: MatDialogRef<ConfirmDialogComponent>,
              @Inject(MAT_DIALOG_DATA) data: any) {

    if (data) {
       if (data.title) {
         this.title = data.title
       }
       if (data.message) {
        this.message = data.message
      }
    }
    this.dialogRef.keydownEvents().subscribe(event => {
      if (event.key === "Escape") {
          this.close();
      }
  });
  }

  confirm() {
    this.dialogRef.close({confirmed: true});
  }

  close() {
        this.dialogRef.close();
  }

}
