import { Component } from '@angular/core';
import { htroot } from '../htroot';

@Component({
  selector: 'app-form-notification',
  templateUrl: './form-notifications.component.html'
})
export class FormNotificationsComponent {
  infoMsg: string;
  errorMsg: string;
  warningMsg: string;
  successMsg: string;
  fieldErrors:any[]

  showOnlyInfo(msg) {
    this.clearNotificacions();
    this.infoMsg = msg;
  }

  showOnlyError(msg) {
    this.clearNotificacions();
    this.errorMsg = msg;
  }

  showOnlyWarning(msg) {
    this.clearNotificacions();
    this.warningMsg = msg;
  }

  clearNotificacions() {
    this.infoMsg = null;
    this.errorMsg = null;
    this.warningMsg = null
    this.successMsg = null;
    this.fieldErrors = []
  }

  showSaveError = function (response) {
    this.showOnlyError(htroot.getErrorMessage(response));
    console.log("SHOW ERROR: ", response)
    this.fieldErrors = response.error.fieldErrors
  }

  hideInfoAlert(){
    this.infoMsg=null;
  }

  hideErrorAlert(){
    this.errorMsg=null;
  }
}
