import { ViewChild } from "@angular/core";
import { DataTableDirective } from "angular-datatables";
import { Subject } from "rxjs";
import { htroot } from "./htroot";


export class SimpleDataTable {
    dtOptions: any = {
        pagingType: 'full_numbers',
        pageLength: 20,
        serverSide: false,
        processing: true,
        autoWidth: false,
        language: htroot.lenguaje,
        dom: 'Blfrtip',
        buttons: this.getButtons()
    };

    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    dtTrigger = new Subject();

    rerender(): void {
        if (this.dtElement.dtInstance == null) {
            this.dtTrigger.next();
            return;
        }
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }

    getButtons() {
        
        return [];
    }

    getReportName(){

    }
}