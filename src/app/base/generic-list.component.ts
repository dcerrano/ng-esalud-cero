import { CurrencyPipe } from "@angular/common";
import { AfterViewInit, Injector, OnDestroy, OnInit, ViewChild, Input } from "@angular/core";
import { DataTableDirective } from "angular-datatables";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { GenericService } from "./generic.service";
import { htroot } from "./htroot";
export class DataTablesResponse {
    aaData: any[];
    draw: number;
    recordsFiltered: number;
    recordsTotal: number;
}

export class SelectedDTRow {
    row: Node
    data: any[] | Object
    index: number
    doubleClick?: boolean
}

export class GenericDataTable<T> implements AfterViewInit, OnDestroy, OnInit {
    protected entities: Array<any>;
    //public dtOptions: DataTables.Settings = {};
    // Must be declared as "any", not as "DataTables.Settings"
    dtOptions: any = {};
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtTrigger = new Subject();

    protected currencyPipe: CurrencyPipe;
    selectedRow: BehaviorSubject<SelectedDTRow> = new BehaviorSubject(new SelectedDTRow());
    newRecordSubject = new Subject();
    @Input() hideButtons = false;
    @Input() hideAddRecordButton = false;
    @Input() hideExcelButton = false;
     serverSide = true
    constructor(protected service: GenericService<T>) {
        this.currencyPipe = Injector.create([{ provide: CurrencyPipe, useClass: CurrencyPipe, deps: [] }]).get(CurrencyPipe);
    }


    ngOnInit(): void {
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 20,
            searchDelay: 2000,
            serverSide: that.serverSide,
            processing: true,
            autoWidth: false,
            columns: this.getInternalColumns(),
            columnDefs: this.getColumnsDefs(),
            language: htroot.lenguaje,
            rowCallback: (row: Node, data: any[] | Object, index: number) => {

                $('td', row).unbind('click');
                $('td', row).bind('click', () => {
                    that.onSelectRow(row, data, index)
                });

                $('td', row).unbind('dblclick');
                $('td', row).bind('dblclick', () => {
                    that.onSelectRow(row, data, index, true)
                });
                this.customRowCallback(row, data, index)
                return row;
            },
            ajax: (dataTablesParameters: any, callback) => {
                that.appendDataTableParameters(dataTablesParameters);
                that.getServerData(dataTablesParameters, callback);
            },
            // Declare the use of the extension in the dom parameter
            dom: 'Blfrtip',
            // Configure the buttons
            buttons: this.getButtons()
        };
        this.setDtExtraOptions()
    }

    customRowCallback (row: Node, data: any, index: number) {

    }

    onSelectRow(row, data, index, doubleClick?:boolean) {
        this.selectedRow.next({ row, data, index , doubleClick});
    }

    setDtExtraOptions(){

    }

    appendDataTableParameters(dataTablesParameters) {

    }

    getServerData(dataTablesParameters:any, callback){
        this.service.dataTable(dataTablesParameters).subscribe(resp => {
            this.afterGetServerData(resp)
            this.entities = resp.aaData;
            callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsFiltered,
                data: resp.aaData
            });
        });
    }

    afterGetServerData(resp){

    }

    getButtons() {
        if (this.hideButtons) {
            return [];
        }
        const that = this;
        let buttons = [];
          /*
        if (!this.hideExcelButton) {
            buttons.push({
                extend: "excelHtml5", filename: that.getReportName(),
                exportOptions: {
                    columns: ':visible'
                },
                exportData: { decodeEntities: true }
            });
        }
         */
        if (!this.hideAddRecordButton) {
            buttons.push({
                text: '+ Agregar Registro',
                key: '1',
                action: function (e, dt, node, config) {
                    that.newRecordSubject.next();
                }
            });
        }


        return buttons;
    }

    getReportName() {
        return ((<any>this).constructor.name as string).replace("Component", "");
    }

    getNewRecordSubject() {
        return this.newRecordSubject.asObservable();
    }

    private getInternalColumns() {
        let columns = this.getColumns();
        columns.forEach(column => {
            if (!column.render) {
                column.render = data => {
                    return htroot.htmlSanitize(data);
                }
            }
        });
        return columns;
    }

    getColumns() {
        return [];
    }

    getColumnsDefs() {
        return [];
    }

    public getSelectedRow(): Observable<SelectedDTRow> {
        return this.selectedRow.asObservable();
    }

    ngAfterViewInit(): void {
        this.dtTrigger.next();
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }

    rerender(): void {
        if (this.dtElement.dtInstance == null) {
            return;
        }
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }


    currencyRender(data) {
        return this.currencyPipe.transform(data, '', '', '1.0-0');
    }

    moneyColumn(data: string, title?: string) {
        if (!title) {
            title = data.toUpperCase();
        }
        return {
            title: title, data: data, className: 'dt-right', render: data => {
                return this.currencyRender(data);
            }
        }
    }
}


export class GenericListComponent<T> extends GenericDataTable<T> {

    constructor(protected service: GenericService<T>) {
        super(service);
    }

    setFieldValue(fieldName, value) {
       this[fieldName] = value
    }
}