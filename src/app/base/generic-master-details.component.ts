import { OnInit, ViewChild } from '@angular/core';
import { FormNotificationsComponent } from './form-notifications/form-notifications.component';
import { GenericService } from './generic.service';
import { BaseComponent } from './base.component';


export class ApiResp<T> {
    data: T;
}

export class GenericMasterDetailComponent<T> extends BaseComponent implements OnInit {

    @ViewChild(FormNotificationsComponent) protected formNotifCmp: FormNotificationsComponent;
    list: ApiResp<T>[] = [];

    constructor(protected service: GenericService<T>, protected formId: string) {
         super();
    }

    ngOnInit() {
        
    }

    add() {
        var apiResp = new ApiResp<T>();
        apiResp.data = this.newEntity();
        this.list.push(apiResp);
    }

    newEntity(): T {
        return {} as T
    }

    save() {
        this.service.saveList($('#' + this.formId).serialize()).subscribe(resp => {
            this.formNotifCmp.showOnlyInfo('Registro guardado con éxito');
        }, resp => {
            this.formNotifCmp.showSaveError(resp);
        });
    }

    remove(data) {
        var index = this.list.indexOf(data);
        if (index > -1) {
            this.list.splice(index, 1);
        }
    }

    compareEntityFn(item, selected) {
        if (selected) {
            return item.id == selected.id;
        }
        return false;
    }
}


export class SingleListComponent<T> extends BaseComponent implements OnInit {

    list: Array<T> = [];

    constructor(protected service: GenericService<T>) {
         super();
    }

    ngOnInit() {
        
    }

    add() {
        this.list.push(this.newEntity());
        this.afterAdd();
    }

    afterAdd(){

    }

    newEntity(): T {
        return {} as T
    }

    remove(data) {
        var index = this.list.indexOf(data);
        if (index > -1) {
            this.list.splice(index, 1);
        }
        this.afterRemove();
    }

    afterRemove(){

    }

    compareEntityFn(item, selected) {
        if (selected) {
            return item.id == selected.id;
        }
        return false;
    }
}



