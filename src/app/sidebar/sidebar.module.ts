import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NavbarModule } from '../shared/navbar/navbar.module';
import { SidebarComponent } from './sidebar.component';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatSelectModule } from '@angular/material';

@NgModule({
    imports: [ RouterModule, CommonModule, MatButtonModule , MatSelectModule, NavbarModule, FormsModule ],
    declarations: [ SidebarComponent ],
    exports: [ SidebarComponent ]
})

export class SidebarModule {

    
}
