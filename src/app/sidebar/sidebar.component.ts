import { Component, OnInit, AfterViewInit } from "@angular/core";
import PerfectScrollbar from "perfect-scrollbar";
import { HTTPStatus } from "../base/interceptors";
import { delay } from "rxjs/operators";
import { SessionService } from "app/base/session.service";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";
import { SessionInfo } from "app/models/common.models";

declare const $: any;

//Metadata
export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  icontype: string;
  collapse?: string;
  permissionName?: string;
  visible?: boolean;
  children?: ChildrenItems[];
}

export interface ChildrenItems {
  path: string;
  title: string;
  ab?: string;
  visible?: boolean;
  ignoreRoot?: boolean
  permissionName?: string;
  permissionList?: string[],
  type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [
  {
    path: "/dashboard",
    title: "Dashboard",
    type: "link",
    icontype: "dashboard",
    permissionName: "Dashboard_sel"
  },
  {
    path: "/dg",
    title: "Datos Generales",
    type: "sub",
    icontype: "apps",
    collapse: "dg",
    children: [
      { path: "segurosmeds", title: "Seguros médicos", permissionName: "SeguroMed_sel" },
      { path: "patologias", title: "Patologias", permissionName: "SeguroMed_sel" },
    ]
  },

  {
    path: "/admin",
    title: "Administración",
    type: "sub",
    icontype: "settings",
    collapse: "admin",
    children: [
      { path: 'usuarios', title: 'Usuarios', permissionName: "User_sel" },
      { path: "roles", title: "Roles", permissionName: "Role_sel" },
    ]
  },

];

@Component({
  selector: "app-sidebar-cmp",
  templateUrl: "sidebar.component.html"
})
export class SidebarComponent implements OnInit, AfterViewInit {
  public menuItems: RouteInfo[] = [];
  public HTTPActivity: boolean;
  sessionInfo: SessionInfo
  userIsDemo = false
  modVentaEnabled = false
  modEncuestaEnabled = false
  sessionServiceSubs: Subscription;

  constructor(
    private httpStatus: HTTPStatus,
    private sessionService: SessionService,
    private router: Router,
  ) { }

  ngAfterViewInit() {
    this.httpStatus
      .getHttpStatus()
      .pipe(delay(100))
      .subscribe((status: boolean) => {
        this.HTTPActivity = status;
      });
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  compareEntityFn(item, selected) {
    if (selected) {
      return item.id === selected.id;
    }
    return false;
  }

  onRefresh() {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    const currentUrl = this.router.url + "?";

    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }

  ngOnInit() {
    this.sessionInfo = this.sessionService.getSessionInfo();
    if (this.sessionInfo) {
      this.loadRoutes();
    } else {
      this.sessionServiceSubs = this.sessionService
        .getSubjectSessionInfo()
        .subscribe(sessionInfo => {
          this.sessionInfo = sessionInfo;
          this.loadRoutes();
        });
    }
  }

  loadRoutes() {
  
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.menuItems.forEach(menuItem => {

      if (menuItem.children) {
        let hasModulePermission = false
        menuItem.children.forEach(child => {
          if (child.permissionName || child.permissionList) {
            if (child.permissionList) {
              child.permissionList.forEach(per => {
                let ok = this.hasPermission(per)
                if (ok) {
                  child.visible = ok;
                  return;
                }
              })
              return
            }
            child.visible = this.hasPermission(child.permissionName, child.ignoreRoot)        
            if (child.visible) {
              hasModulePermission = true
            }
          } else {
            hasModulePermission = true
            child.visible = true
          }
        })
        menuItem.visible = hasModulePermission
      } else {
        if (menuItem.permissionName) {
          menuItem.visible = this.hasPermission(menuItem.permissionName)
        } else {
          menuItem.visible = true
        }
      }

    });
  }

  hasPermission(permissionName, ignoreRoot?) {
    return this.sessionService.hasPermission(permissionName, ignoreRoot);
  }

  updatePS(): void {
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      const elemSidebar = <HTMLElement>(
        document.querySelector(".sidebar .sidebar-wrapper")
      );
      let ps = new PerfectScrollbar(elemSidebar, {
        wheelSpeed: 2,
        suppressScrollX: true
      });
    }
  }
  isMac(): boolean {
    let bool = false;
    if (
      navigator.platform.toUpperCase().indexOf("MAC") >= 0 ||
      navigator.platform.toUpperCase().indexOf("IPAD") >= 0
    ) {
      bool = true;
    }
    return bool;
  }

  logout() {
    this.sessionService.logout().subscribe(resp => {
      this.router.navigate(["pages/login"]);
    });
  }


  ngOnDestroy() {
    if (this.sessionServiceSubs) {
      this.sessionServiceSubs.unsubscribe();
    }
  }
}
