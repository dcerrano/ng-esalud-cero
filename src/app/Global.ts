export class Global {
    public static URL_BASE = 'http://localhost:8080/esalud-backend/';
    public static LOGIN_URL = Global.URL_BASE + 'login';
    public static LOGOUT_URL = Global.URL_BASE + 'cerrar-sesion';
    public static SESSION_INFO_URL = Global.URL_BASE + 'session/info';
}

// ng build --prod --base-href=/esalud/ --output-hashing=all