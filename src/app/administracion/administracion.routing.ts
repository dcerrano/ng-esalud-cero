import { Routes } from '@angular/router';
import { UsuarioComponent } from './usuario/usuario.component';
import { RoleComponent } from './rol/rol.component';

export const AdministracionsRoutes: Routes = [
  {
    path: '',
    children: [
      { path: 'usuarios', component: UsuarioComponent },
      { path: 'roles', component: RoleComponent },
    ]
  }
];
