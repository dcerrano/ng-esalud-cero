
export class Role {
    id: number;
    code: string;
    description:string
}

export class RolPermiso {
    id: number;
    code: string;
    description:string
    checked:string
}

export class Usuario {
    id: number = null;
    username: string;
    nombres: string;
    apellidos: string;
    email:string;
    role:Role
}

export class LoginInfo {
    id: number = null;
    username: string;
    name: string;
    surname: string;
}

