import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { GenericService } from "../base/generic.service";
import { Usuario, Role, RolPermiso, LoginInfo } from "./administracion.models";
import { Global } from "app/Global";

@Injectable()
export class RoleService extends GenericService<Role> {
  constructor(http: HttpClient) {
    super(http, "roles");
  }
}

@Injectable()
export class RolPermisoService extends GenericService<RolPermiso> {
  constructor(http: HttpClient) {
    super(http, "role-permissions");
  }

  grantPermission(roleId, permissionId) {
    return this.http.post(
      Global.URL_BASE + "roles/grant-permission/" + roleId + "/" + permissionId,
      null
    );
  }

  revokePermission(roleId, permissionId) {
    return this.http.delete(
      Global.URL_BASE + "roles/revoke-permission/" + roleId + "/" + permissionId
    );
  }
}

@Injectable()
export class UsuarioService extends GenericService<Usuario> {
  constructor(http: HttpClient) {
    super(http, "users");
  }

  save(fd) {
    return this.http.post(this.url + "/add", fd);
  }

  getImageUrl(id) {
    return this.url + "/pic/" + id + ".png"
  }

  cambiarPass(userId, newPass) {
    const fd = new FormData();
    fd.append("userId", userId);
    fd.append("password", newPass);
    return this.http.post(this.url+'/changepass', fd);
  }
}

@Injectable()
export class LoginInfoService extends GenericService<LoginInfo> {
  constructor(http: HttpClient) {
    super(http, "logininfo");
  }
}

