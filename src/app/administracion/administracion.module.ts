import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { MaterialModule } from '../app.module';
import { BaseModule } from '../base/base.module';
import { AdministracionsRoutes } from './administracion.routing';
import { LoginInfoService, RoleService, RolPermisoService, UsuarioService } from './administracion.services';
import { RoleFormComponent } from './rol/rol-form.component';
import { RoleListComponent } from './rol/rol-list.component';
import { RolPermisoListComponent } from './rol/rol-permiso-list.component';
import { RoleComponent } from './rol/rol.component';
import { LoginInfoListComponent } from './usuario/logininfo-list.component';
import { UsuarioFormComponent } from './usuario/usuario-form.component';
import { UsuarioListComponent } from './usuario/usuario-list.component';
import { UsuarioPickerComponent } from './usuario/usuario-picker.component';
import { UsuarioComponent } from './usuario/usuario.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdministracionsRoutes), FormsModule, ReactiveFormsModule, 
    MaterialModule, DataTablesModule, BaseModule,  Ng2AutoCompleteModule, CurrencyMaskModule
  ],
  providers: [RoleService, UsuarioService, LoginInfoService, RolPermisoService],

  declarations: [
  UsuarioComponent, UsuarioListComponent, UsuarioFormComponent, UsuarioPickerComponent, LoginInfoListComponent,
RoleComponent, RoleListComponent, RoleFormComponent, RolPermisoListComponent]
})
export class AdministracionModule {

}
