import { Component } from "@angular/core";
import { GenericListComponent } from "../../base/generic-list.component";
import { LoginInfo } from "../administracion.models";
import { LoginInfoService } from "../administracion.services";

@Component({
  selector: 'app-logininfo-list',
  template: '<div class="table-responsive"><table datatable [dtOptions]="dtOptions" [dtTrigger]="dtTrigger" class="table row-border hover"></table></div>'
})
export class LoginInfoListComponent extends GenericListComponent<LoginInfo> {

  constructor(private loginService: LoginInfoService) {
    super(loginService);
    this.hideAddRecordButton=true
  }

  setDtExtraOptions() {
    this.dtOptions["order"] = [[3, "desc"]];
  }

  getColumns() {
    return [
      { title: 'USUARIO', data: 'username', width: '10%', className: 'dt-right' },
      { title: 'NOMBRES', data: 'name' },
      { title: 'APELLIDOS', data: 'surname' },
      { title: 'FECHA', data: 'date', width: '20px', className: 'dt-center' },
      { title: 'IP', data: 'ip', width: '20px', className: 'dt-right' }
    ];
  }
}


