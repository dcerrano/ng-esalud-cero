import { Component,  ViewChild } from '@angular/core';
import { GenericListFormComponent } from '../../base/generic-list-form.component';
import { Usuario } from '../administracion.models';
import { UsuarioFormComponent } from './usuario-form.component';
import { UsuarioListComponent } from './usuario-list.component';
import { UsuarioService } from '../administracion.services';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html'
})
export class UsuarioComponent extends GenericListFormComponent<Usuario> {
  @ViewChild(UsuarioListComponent) list: UsuarioListComponent;
  @ViewChild(UsuarioFormComponent) form: UsuarioFormComponent;

  usuario: Usuario;
  constructor(private service: UsuarioService) {
    super();
  }

  onSelectedRow(selectedDTRow) {
    this.service.findOne(selectedDTRow.data['id']).subscribe((func) => {
      this.selectedEntity = func;
      this.usuario = func;
      this.form.setEntity(func);
      this.setSelectedTab(2);
    })
  }
 
}

