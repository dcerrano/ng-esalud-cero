
import { Component, ViewChild } from '@angular/core';
import { GenericPickerComponent } from '../../base/generic-picker.component';
import { ModalComponent } from '../../base/modal/modal.component';
import { Usuario } from '../administracion.models';
import { UsuarioListComponent } from './usuario-list.component';

@Component({
    selector: 'app-usuario-picker',
    templateUrl: './usuario-picker.component.html',
})
export class UsuarioPickerComponent extends GenericPickerComponent<Usuario> {

    @ViewChild(UsuarioListComponent) usuarioListComponent: UsuarioListComponent
    @ViewChild('usuarioListModal') usuarioListModal: ModalComponent

    setSelectedListItem(item) {
        this.entity = new Usuario();
        this.entity.id = item.id;
    }

    getListComponent() {
        return this.usuarioListComponent;
    }

    getListModal() {
        return this.usuarioListModal;
    }

    reset() {
        this.entity = new Usuario();
    }
}
