import { Component } from "@angular/core";
import { GenericListComponent } from "../../base/generic-list.component";
import { Usuario } from "../administracion.models";
import { UsuarioService } from "../administracion.services";

@Component({
  selector: 'app-usuario-list',
  template: '<div class="table-responsive"><table datatable [dtOptions]="dtOptions" [dtTrigger]="dtTrigger" class="table row-border hover"></table></div>'
})
export class UsuarioListComponent extends GenericListComponent<Usuario> {

  constructor(service: UsuarioService) {
    super(service);
  }

  getColumns() {
    return [
      { title: 'USUARIO', data: 'username', width: '10%', className: 'dt-right' },
      { title: 'NOMBRES', data: 'name' },
      { title: 'APELLIDOS', data: 'surname' },
      { title: 'EMAIL', data: 'email' }
    ];
  }
}


