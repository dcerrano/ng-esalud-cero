import { Component, ViewChild } from '@angular/core';
import { GenericFormComponent } from '../../base/generic-form.component';
import { Usuario, Role } from '../administracion.models';
import { UsuarioService, RoleService } from '../administracion.services';
import { ModalComponent } from 'app/base/modal/modal.component';
import { htroot } from 'app/base/htroot';

@Component({
    selector: 'app-usuario-form',
    templateUrl: './usuario-form.component.html'
})
export class UsuarioFormComponent extends GenericFormComponent<Usuario> {

    @ViewChild('cambiarPassModal') cambiarPassModal: ModalComponent


    roleList: Array<Role> = []

    newPass: String;
    confirmPass: String

    constructor(public service: UsuarioService, private roleService: RoleService) {
        super(service);
        this.roleService.suggest().subscribe(resp => {
            this.roleList = resp;
        });
    }

    newEntity() {
        let usuario = new Usuario();
        return usuario;
    }


    cambiarPass() {
        this.service.cambiarPass(this.entity.id, this.newPass).subscribe(ok => {
            this.cambiarPassModal.close()
            htroot.showInfoPopup('Cambio de contraseña', 'Contraseña cambiada con éxito')
        }, error => {
            htroot.showErrorPopup('Cambio de contraseña', 'Ocurrió un error al intentar cambiar la contraseña')
        })
    }
    
    afterSetEntity(entity) {
        if (this.entity.username == 'owl') {
            this.abmButtonsCmp.saveBtnEnabled = false
            this.abmButtonsCmp.deleteBtnEnabled = false
        }
        return entity;
    }
}
