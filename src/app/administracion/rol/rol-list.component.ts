import { Component } from "@angular/core";
import { GenericListComponent } from "../../base/generic-list.component";
import { Role } from "../administracion.models";
import { RoleService } from "../administracion.services";

@Component({
  selector: 'app-rol-list',
  template: ' <table datatable [dtOptions]="dtOptions" [dtTrigger]="dtTrigger" class="table row-border hover"></table>'
})
export class RoleListComponent extends GenericListComponent<Role> {

  constructor(service: RoleService) {
    super(service);
    this.serverSide=false
  }

  getColumns() {
    return [
      { title: 'CÓDIGO', data: 'code', width:'50px' },
      { title: 'DESCRIPCIÓN', data: 'description' }
    ];
  }
  getServerData(dataTablesParameters: any, callback) {
    this.service.getList().subscribe((resp: any) => {
      this.entities = resp;
      callback({
        recordsTotal: resp.length,
        recordsFiltered: resp.length,
        data: resp
      });
    });
  }
}


