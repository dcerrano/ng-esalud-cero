import { Component, ViewChild } from '@angular/core';
import { GenericFormComponent } from '../../base/generic-form.component';
import { Role } from '../administracion.models';
import { RoleService } from '../administracion.services';
import { RolPermisoListComponent } from './rol-permiso-list.component';

@Component({
    selector: 'app-rol-form',
    templateUrl: './rol-form.component.html'
})
export class RoleFormComponent extends GenericFormComponent<Role> {

    @ViewChild(RolPermisoListComponent) rolPermisoListComponent: RolPermisoListComponent;

    constructor(public service: RoleService) {
        super(service);
    }

    newEntity() {
        let rol = new Role();
        return rol;
    }

    patchValue(entity) {
        this.entity = entity;
       this.rolPermisoListComponent.setRol(this.entity);
    }
}
