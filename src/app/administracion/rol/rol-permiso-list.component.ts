import { Component } from "@angular/core";
import { GenericListComponent } from "../../base/generic-list.component";
import { RolPermiso, Role } from "../administracion.models";
import { RolPermisoService } from "../administracion.services";

@Component({
  selector: 'app-rol-permiso-list',
  template: ' <table datatable [dtOptions]="dtOptions" [dtTrigger]="dtTrigger" class="table row-border hover"></table>'
})
export class RolPermisoListComponent extends GenericListComponent<RolPermiso> {

  rol: Role;
  constructor(protected service: RolPermisoService) {
    super(service);
  }

  getColumns() {
    var self = this;
    return [
      { title: 'CÓDIGO', data: 'code', width: '50px' },
      { title: 'DESCRIPCIÓN', data: 'description' },
      {
        title: 'ASIGNADO', data: 'checked', width: '50px', className: 'dt-center', createdCell: function (cell, cellData, rowData) {
          if (cellData == 'SI') {
            var check = $("<input type='checkbox' checked='checked'/>")
          } else {
            var check = $("<input type='checkbox'/>")
          }

          $(cell).html("");
          $(cell).append(check);
          $(check).unbind('click');
          $(check).bind('click', (event) => {
            console.log("rowData.checked:", rowData.checked)
            if (rowData.checked == 'SI') {
              rowData.checked = 'NO'
              self.service.revokePermission(self.rol.id, rowData.id).subscribe(resp => {
                console.log("Baja permiso");
              });
            } else {
              rowData.checked = 'SI'

              self.service.grantPermission(self.rol.id, rowData.id).subscribe(resp => {
                console.log("Alta permiso");
              });
            }
          });
        }
      }
    ];
  }


  appendDataTableParameters(dataTablesParameters) {
    if (this.rol) {
      dataTablesParameters.roleId = this.rol.id;
    }
  }

  setRol(rol) {
    this.rol = rol;
    this.rerender();
  }

}


