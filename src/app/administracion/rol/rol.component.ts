import { Component, ViewChild } from '@angular/core';
import { GenericListFormComponent } from '../../base/generic-list-form.component';
import { Role } from '../administracion.models';
import { RoleService } from '../administracion.services';
import { RoleFormComponent } from './rol-form.component';
import { RoleListComponent } from './rol-list.component';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html'
})
export class RoleComponent extends GenericListFormComponent<Role> {
  @ViewChild(RoleListComponent) list: RoleListComponent;
  @ViewChild(RoleFormComponent) form: RoleFormComponent;

  constructor(private service: RoleService) {
    super();
  }

}
